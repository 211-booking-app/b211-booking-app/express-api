const Course = require("../models/Course");

//Mini Activity
//Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

// module.exports.addCourse = (reqBody) => {

// 	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
// 	// Uses the information from the request body to provide all the necessary information
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	// Saves the created object to our database
// 	return newCourse.save().then((course, error) => {

// 		// Course creation successful
// 		if (error) {

// 			return false;

// 		// Course creation failed
// 		} else {

// 			return true;

// 		};

// 	});

// };

/*
	1. Create a conditional statement that will check if the user is an admin
	2. Create a new Course object using mongoose model and the information the reqBody
	3. Save the new User to the database


*/
//Solution 1

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	if(reqBody.isAdmin){

// 		return newCourse.save().then((course, error) => {

// 		if (error) {

// 			return false;

// 		} else {

// 			return true;

// 		};

// 	});

// 	}else{
// 		return false;
// 	}
// };

//Solution 2

// Create a new course
/*
	Steps:
	1. Create a conditional statement that will check if the user is an administrator.
	2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	3. Save the new User to the database
*/

module.exports.addCourse = (data) => {
	// User is an admin
	console.log(data);

	if(data.isAdmin){
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		console.log(newCourse);
		// Saves the created object to our databas
		return newCourse.save().then((course, error) => {
		// Course creation successful
		if (error) {

			return false;
		// Course creation failed
		} else {

			return true;

		};

	});
	// User is not an admin
	}else{
		return false;
	};
};

//Retrieve all courses
/*
	1.Retrieve all the courses from the database
		Model.find({})
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	});
};


//Retrieve all Active Courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true

*/

module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	});
};

//Retrieving a specific course
/*
	1.Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};

//Update a course
/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams,reqBody) =>{
	// Specify the fields/properties of the document to be updated
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	// Syntax
		// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		// Course not updated
		if(error){
			return false;
		// Course updated successfully
		}else{
			return true;
		};
	});
};

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (reqParams) => {

	console.log(reqParams);

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};

//solution 2
// module.exports.archiveCourse = (reqParams)=>{
// 	return Course.findByIdAndUpdate(reqParams,{isActive:false}).then((course,error)=>{
// 		if(error){
// 			return false
// 		}else{
// 			return true
// 		}
// 	});
// };

//stretch goal

module.exports.activateCourse = (reqParams) => {

	console.log(reqParams);

	let updateActiveField = {
		isActive : true
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;
			console.log(`${courseId} archived!`)

		}

	});
};