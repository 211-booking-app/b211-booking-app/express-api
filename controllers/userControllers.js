// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
//bcrypt is a package which allows us to hash our passwords to add a layer of security for our user's details

//Check if the email already exists
/*
	Steps"
	1. Use mongoose "find" method to find duplicate emails
	2.Use the "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) =>{
	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email:reqBody.email}).then(result=>{
		// The "find" method returns a record if a match is found
		if(result.length>0){
			return true;
		// No duplicate email found
		// The user is not yet registered in the databas
		}else{
			return false;
		};
	});
};

//User Registration
/*
	Steps:
	1. Create a new User object using the mongoose modela and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>{

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password,10)
	})
	// Saves the created object to our database
	return newUser.save().then((user,error)=>{
		// User registration failed
		if(error){
			return false;
		// User registration successful
		}else{
			return true;
		};
	});
};

//User Authentication
/*
	Steps:
	1. Check the database if the user email exists
	2.Compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web token if the user is successfully logged in and return false if not

*/

module.exports.loginUser = (reqBody) =>{
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteri	
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			// User does not exist
			return false;
			// User exists
		}else{
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
				//example. isSingle, isDone, isAdmin, areDone, etc..
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			// If the passwords match/result of the above code is true
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return { access: auth.createAccessToken(result)}
			// Passwords do not match
			}else{
				return false
			};
		};
	});
};

//Retrieve user details
/*
	Steps:
	1. Find the document in the db using the user's ID
	2. Reassign the pw of the returned document to an empty string
	3. Return the result back to the frontend
*/
//Solution 1
// module.exports.getProfile = (reqBody) =>{
// 	return User.findById(reqBody.id).then(result=>{
// 		result.password = "";
// 		return result;
// 	})
// }

module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		//*****remove setting the password as null
		// Returns the user information with the password as an empty string
		return result;
	})
}

//Enroll user to a class
/*
	Steps:
	1.Find the document in the database using the user's ID
	2.Add the courseID to the user's enrollement array
	3. Update the doucmnet in MongoDB

*/

/*
	First, find the user who is enrolling and update his enrollments subdocumnet array. We will push the courseID in the enrollments array

	Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

	Since we will access 2 collections in one action -- we will have to wait for the completion of the action instead of letting Javascript continue line per line
	
	To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding

*/

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) =>{

	

	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend

	//The await keyword can only be used inside an async function.	

	let isUserUpdated = await User.findById(data.userId).then(user=>{

		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId:data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});

	});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId:data.userId});

		// Saves the updated course information in the database
		return course.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated&&isCourseUpdated){
		console.log(`User ${data.userId} enrolled ${data.courseId} course`)
		return true;
	// User enrollment failure
	}else{
		return false;
	};
};